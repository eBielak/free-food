import React from 'react';
import NotFound, { sorryText } from '../../components/NotFound';

describe('<NotFound />', () => {
  it(`schould render NotFound component with text "${sorryText}"`, () => {
    const wrapper = mount(<NotFound />);

    expect(wrapper.text()).toEqual(sorryText);
    expect(wrapper).toMatchSnapshot();
  });
});
