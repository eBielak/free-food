import React from 'react';
import Table from '../../components/Table';
import clientsOrdersList from '../../helpers/clientsOrdersList';

const TableData = {
  thead: [
    {
      name: 'Name',
      align: 'left'
    },
    {
      name: 'Surname',
      align: 'left'
    },
    {
      name: 'Orders',
      align: 'left'
    },
    {
      name: 'Total',
      align: 'right'
    }
  ],
  clients: clientsOrdersList
};

describe('<Table />', () => {
  it('schould render ClientOrders component with clients list', () => {
    const wrapper = mount(<Table data={TableData} />);

    // get all clients from props
    const TableDataProps = wrapper.props().data;

    // schould have one table
    const table = wrapper.find('table');
    expect(table).toHaveLength(1);

    // schould have one thead
    const thead = table.find('thead');
    expect(thead).toHaveLength(1);

    // schould have one tbody
    const tbody = table.find('tbody');
    expect(tbody).toHaveLength(1);

    // tbody schould have exacly the same tr as clients
    const rows = tbody.find('tr');
    expect(rows).toHaveLength(TableDataProps.clients.length);

    // clients schould looks like test array of clients
    expect(TableDataProps.clients).toEqual(expect.arrayContaining(clientsOrdersList));

    expect(wrapper).toMatchSnapshot();
  });
});
