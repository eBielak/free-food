import React from 'react';
import Header from '../../layouts/Header';

describe('<Header />', () => {
  it('schould render Header component with h1 with text "Orders grouped by clients', () => {
    const wrapper = mount(<Header />);

    expect(wrapper.find('h1')).toHaveLength(1);
    expect(wrapper.text()).toEqual('Orders grouped by clients');
    expect(wrapper).toMatchSnapshot();
  });
});
