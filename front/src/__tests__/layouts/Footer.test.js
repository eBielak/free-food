import React from 'react';
import Footer, { getCopyrightInfo } from '../../layouts/Footer';

describe('<Footer />', () => {
  it('schould render Footer component with copyright text', () => {
    const wrapper = mount(<Footer />);

    expect(wrapper.text()).toEqual(getCopyrightInfo());
    expect(wrapper).toMatchSnapshot();
  });
});
