import clientsReducer from '../../reducers/clients';
import { SET_CLIENTS_ORDERS } from '../../helpers/consts';
import clientOrdersList from '../../helpers/clientsOrdersList';

const initialState = { data: [], loading: true };

describe('clients reducer', () => {
  it('has a default state', () => {
    expect(clientsReducer(undefined, { type: 'none' })).toEqual(initialState);
  });

  it('schould save clients data and change loading to false', () => {
    expect(clientsReducer(undefined, { type: SET_CLIENTS_ORDERS, data: clientOrdersList })).toEqual({
      data: clientOrdersList,
      loading: false
    });
  });
});
