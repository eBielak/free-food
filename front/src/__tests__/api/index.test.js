import mockAxios from 'axios';
import clientsOrders from '../../helpers/clientsOrdersList';
import Api from '../../api';

const api = new Api();

describe('Api test', () => {
  it('schould call mockup axios and returned list of clients', async () => {
    mockAxios.get.mockImplementationOnce(() => {
      return Promise.resolve({
        data: clientsOrders
      });
    });

    const result = await api.getClientsOrders();
    expect(result.data[0]._id).toEqual('5d420bd163011778d527039d');
    expect(result.data[0].totalAmount).toEqual(244);
    expect(result.data[1]._id).toEqual('5d420acf2fe18d6344267454');
    expect(result.data[1].totalAmount).toEqual(454);
    expect(result.data.length).toEqual(2);
    expect(mockAxios.get).toHaveBeenCalledTimes(1);
  });
});
