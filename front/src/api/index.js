import axios from 'axios';

export default class Api {
  constructor() {
    this.api = axios.create({
      baseURL: process.env.API_URL
    });
  }

  async getClientsOrders() {
    return await this.api.get(`/clients-orders`);
  }
}
