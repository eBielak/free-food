import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import Table from '../components/Table';
import NotFound from '../components/NotFound';
import LoadingSpinner from '../components/LoadingSpinner';

import { getClientsOrders } from '../actions';

const ClientsOrdersContainer = () => {
  const dispatch = useDispatch();
  const clients = useSelector(state => state.clients);

  useEffect(() => {
    dispatch(getClientsOrders());
  }, []);

  const TableData = {
    thead: [
      {
        name: 'Name',
        align: 'left'
      },
      {
        name: 'Surname',
        align: 'left'
      },
      {
        name: 'Orders',
        align: 'left'
      },
      {
        name: 'Total',
        align: 'right'
      }
    ],
    clients: clients.data
  };

  const getContent = () => {
    if (clients.loading) {
      return <LoadingSpinner />;
    } else if (!clients.loading && clients.data.length === 0) {
      return <NotFound />;
    } else {
      return <Table data={TableData} />;
    }
  };

  return getContent();
};

export default ClientsOrdersContainer;
