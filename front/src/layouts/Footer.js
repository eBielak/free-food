import React from 'react';

import styled from 'styled-components';

const StyledFooter = styled.footer`
  text-align: center;
  padding: 10px 20px;
  margin-top: 40px;
  font-size: 12px;
  color: #607d8b;
`;

export const getCopyrightInfo = () => {
  return `${new Date().getFullYear()} - This app was made for AppStract recrutations`;
};

const Footer = () => {
  return <StyledFooter>{getCopyrightInfo()}</StyledFooter>;
};

export default Footer;
