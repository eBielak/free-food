import React from 'react';
import styled from 'styled-components';

const StyledHeader = styled.header`
  padding: 20px;
  text-align: center;

  h1 {
    font-weight: bold;
    text-transform: uppercase;
    font-weight: bold;
  }
`;

const Header = () => {
  return (
    <StyledHeader>
      <h1>Orders grouped by clients</h1>
    </StyledHeader>
  );
};

export default Header;
