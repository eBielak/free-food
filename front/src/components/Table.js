import React from 'react';

import TableRow from './TableRow';
import styled from 'styled-components';

const StyledWrapper = styled.div`
  width: 600px;
  margin: 0 auto;
  padding: 40px;
  border-radius: 10px;
  background: #fff;
  box-shadow: 0px 10px 13px -6px rgba(0, 0, 0, 0.08), 0px 20px 31px 3px rgba(0, 0, 0, 0.09),
    0px 8px 20px 7px rgba(0, 0, 0, 0.02);
`;

const StyledTable = styled.table`
  width: 100%;
`;

const StyledThead = styled.thead`
  color: #999;
  font-size: 14px;
`;

const Table = ({ data }) => {
  return (
    <StyledWrapper>
      <StyledTable cellPadding={10}>
        <StyledThead>
          <tr>
            {data.thead.map((item, index) => (
              <th align={item.align} key={index}>
                {item.name}
              </th>
            ))}
          </tr>
        </StyledThead>
        <tbody>
          {data.clients.map(client => (
            <TableRow key={client._id} data={client} />
          ))}
        </tbody>
      </StyledTable>
    </StyledWrapper>
  );
};

export default Table;
