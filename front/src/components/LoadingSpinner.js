import React from 'react';
import styled, { keyframes } from 'styled-components';

const StyledLoaderWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const ellipsis1 = keyframes`
  0% {
    transform: scale(0);
  }
  100% {
    transform: scale(1);
  }
`;

const ellipsis2 = keyframes`
  0% {
    transform: translate(0, 0);
  }
  100% {
    transform: translate(19px, 0);
  }
`;

const ellipsis3 = keyframes`
  0% {
    transform: scale(1);
  }
  100% {
    transform: scale(0);
  }
`;

const StyledLoader = styled.div`
  display: inline-block;
  position: relative;
  width: 64px;
  height: 64px;

  div {
    position: absolute;
    top: 27px;
    width: 11px;
    height: 11px;
    border-radius: 50%;
    background: #999;
    animation-timing-function: cubic-bezier(0, 1, 1, 0);

    &:nth-child(1) {
      left: 6px;
      animation: ${ellipsis1} 0.6s infinite;
    }

    &:nth-child(2) {
      left: 6px;
      animation: ${ellipsis2} 0.6s infinite;
    }

    &:nth-child(3) {
      left: 26px;
      animation: ${ellipsis2} 0.6s infinite;
    }

    &:nth-child(4) {
      left: 45px;
      animation: ${ellipsis3} 0.6s infinite;
    }
  }
`;

const LoadingSpinner = () => {
  return (
    <StyledLoaderWrapper>
      <StyledLoader>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </StyledLoader>
    </StyledLoaderWrapper>
  );
};

export default LoadingSpinner;
