import React from 'react';
import styled from 'styled-components';

const StyledRow = styled.tr`
  font-size: 18px;
  font-weight: bold;

  td {
    border-top: 1px solid #f1f1f1;
  }
`;

const TableRow = ({ data }) => {
  return (
    <StyledRow>
      <td>{data.client.name}</td>
      <td>{data.client.surname}</td>
      <td>{data.count}</td>
      <td align="right">{data.totalAmount}</td>
    </StyledRow>
  );
};

export default TableRow;
