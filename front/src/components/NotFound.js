import React from 'react';
import styled from 'styled-components';

const StyledNotFound = styled.div`
  text-align: center;
  font-size: 14px;
  font-weight: bold;
`;

export const sorryText = "Sorry, We don't found any Clients...";

const NotFound = () => {
  return <StyledNotFound>{sorryText}</StyledNotFound>;
};

export default NotFound;
