import { GET_CLIENTS_ORDERS, SET_CLIENTS_ORDERS } from '../helpers/consts';

export const getClientsOrders = () => ({ type: GET_CLIENTS_ORDERS });

export const setClientsOrders = data => ({ type: SET_CLIENTS_ORDERS, data });
