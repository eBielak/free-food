import { put, takeLatest } from 'redux-saga/effects';
import { setClientsOrders } from '../actions';
import { GET_CLIENTS_ORDERS } from '../helpers/consts';
import Api from '../api';

const clientsApi = new Api();

export function* getDataFromApi() {
  try {
    const result = yield clientsApi.getClientsOrders();

    if (result.status === 200) {
      yield put(setClientsOrders(result.data));
    }
  } catch (e) {
    console.log('Error ' + e);
  }
}

function* makeRequest() {
  yield takeLatest(GET_CLIENTS_ORDERS, getDataFromApi);
}

export default makeRequest;
