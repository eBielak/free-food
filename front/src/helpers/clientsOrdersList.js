export default [
  {
    _id: '5d420bd163011778d527039d',
    totalAmount: 244,
    count: 3,
    client: [
      {
        _id: '5d420bd163011778d527039d',
        name: 'Jan',
        surname: 'Kowalski',
        __v: 0
      }
    ]
  },
  {
    _id: '5d420acf2fe18d6344267454',
    totalAmount: 454,
    count: 2,
    client: [
      {
        _id: '5d420acf2fe18d6344267454',
        name: 'John',
        surname: 'Walker',
        __v: 0
      }
    ]
  }
];
