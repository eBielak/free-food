import { createGlobalStyle } from 'styled-components';

const GlobalStyles = createGlobalStyle`
  body{
    background: #f1f1f1;
  }`;

export default GlobalStyles;
