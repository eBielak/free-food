import { SET_CLIENTS_ORDERS } from '../helpers/consts';

export default (state = { data: [], loading: true }, { type, data }) => {
  switch (type) {
    case SET_CLIENTS_ORDERS:
      return { data, loading: false };
    default:
      return state;
  }
};
