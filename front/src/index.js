import 'babel-polyfill';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import Home from './pages/Home';
import Header from './layouts/Header';
import Footer from './layouts/Footer';

import GlobalStyles from './helpers/GlobalStyle';
import styled from 'styled-components';

import store from './store';

const StyledWrapper = styled.section`
  display: grid;
  grid-template-areas: 'header' 'content' 'footer';
  font-family: 'Muli', sans-serif;
`;

const App = () => {
  return (
    <Provider store={store}>
      <StyledWrapper>
        <Header />
        <Home />
        <Footer />
      </StyledWrapper>
      <GlobalStyles />
    </Provider>
  );
};

ReactDOM.render(<App />, document.querySelector('#root'));
