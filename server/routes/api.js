import { Router } from 'express';
import { getClientsOrders } from '../controllers/clients';

const router = Router();

router.get('/clients-orders', getClientsOrders);

export default router;
