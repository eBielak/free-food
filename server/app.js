import express from 'express';
import cors from 'cors';
import apiRoute from './routes/api';
import { connectDb } from './database';

require('dotenv/config');

const app = express();

app.use(cors());
app.use('/api/v1', apiRoute);

connectDb()
  .then(() => {
    app.listen(9000);
  })
  .catch(err => {
    console.log(err);
  });
