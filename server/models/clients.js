import mongoose from 'mongoose';

const ClientSchema = mongoose.Schema({
  name: { type: String, required: true },
  surname: { type: String, required: true },
});

export default mongoose.model('Clients', ClientSchema);
