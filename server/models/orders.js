import mongoose from 'mongoose';

const OrderSchema = mongoose.Schema({
  amount: Number,
  clientId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Clients',
  },
});

export default mongoose.model('Orders', OrderSchema);
