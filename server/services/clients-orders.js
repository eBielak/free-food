import Orders from '../models/orders';

export const clientsOrders = async () => {
  const result = await Orders.aggregate(
    [
      {
        $group: {
          _id: '$clientId',
          totalAmount: { $sum: '$amount' },
          count: { $sum: 1 },
        },
      },
      {
        $lookup: {
          from: 'clients',
          localField: '_id',
          foreignField: '_id',
          as: 'client',
        },
      },
    ],
    (err, data) => {
      if (err) throw new Error(err);

      return data;
    },
  );

  return result;
};
