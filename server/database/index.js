import mongoose from 'mongoose';

export const connectDb = () => {
  return mongoose.connect(
    `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@gitjob-jhurp.mongodb.net/${process.env.DB_NAME}?retryWrites=true`,
    {
      useNewUrlParser: true,
    },
  );
};
