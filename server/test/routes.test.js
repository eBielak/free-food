import app from '../app';
import request from 'supertest';

describe('default route', function() {
  it('should return status 404', function() {
    request(app)
      .get('/any-route')
      .expect(404);
  });
});
