import { clientsOrders } from '../services/clients-orders';

export const getClientsOrders = async (_, res) => {
  try {
    const data = await clientsOrders();

    const groupClientsOrders = data.map(result => {
      return {
        ...result,
        client: Object.values(result.client)[0],
      };
    });

    if (groupClientsOrders) res.json(groupClientsOrders);
  } catch (err) {
    console.log(err);
    return res.status(500).send();
  }
};
